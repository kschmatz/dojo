package kds.chart;

class Start extends State {
    @Override
    protected State next(int c) {
        return (compare(c) < 0) ? new SearchLow1(this) : this;
    }
}
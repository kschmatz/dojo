package kds.chart;

class Final extends State {
    Final(State pred) {
        super(pred);
    }

    @Override
    protected State next(int c) {
        return this;
    }
}
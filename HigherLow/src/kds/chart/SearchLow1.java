package kds.chart;

class SearchLow1 extends State {
    SearchLow1(State pred) {
        super(pred);
    }

    @Override
    protected State next(int c) {
        State next = this;
        if (compare(c) > 0) {
            next = new SearchHigh(this);
            next.setLow1(getPrevious());
        }
        return next;
    }
}

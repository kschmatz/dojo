package kds.chart;

abstract class State {
    private int index;
    private int previous;
    private int low1;
    private int entry;

    State() {
        index = 0;
        previous = Integer.MIN_VALUE;
        low1 = Integer.MIN_VALUE;
        entry = -1;
    }

    State(State pred) {
        index = pred.index;
        previous = pred.previous;
        low1 = pred.low1;
        entry = pred.entry;
    }

    protected final int compare(int price) {
        if (price > previous) {
            return 1;
        } else if (price < previous) {
            return -1;
        } else {
            return 0;
        }
    }

    protected final int getEntry() {
        return entry;
    }

    protected final int getIndex() {
        return index;
    }

    protected final int getLow1() {
        return low1;
    }

    protected final int getPrevious() {
        return previous;
    }

    protected final void incrementIndex() {
        index++;
    }

    protected abstract State next(int c);

    protected final State process(int c) {
        State next = next(c);
        next.setPrevious(c);
        next.incrementIndex();
        return next;
    }

    protected final void setEntry(int index) {
        entry = index;
    }

    protected final void setLow1(int price) {
        low1 = price;
    }

    protected final void setPrevious(int price) {
        previous = price;
    }
}
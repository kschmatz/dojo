package kds.chart;

class SearchHigh extends State {
    SearchHigh(State pred) {
        super(pred);
    }

    @Override
    protected State next(int c) {
        return (compare(c) < 0) ? new SearchLow2(this) : this;
    }
}
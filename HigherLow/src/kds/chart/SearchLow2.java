package kds.chart;

class SearchLow2 extends State {
    SearchLow2(State pred) {
        super(pred);
    }

    @Override
    protected State next(int c) {
        State next = this;
        if (compare(c) > 0) {
            if (getLow1() < getPrevious()) {
                next = new Final(this);
                next.setEntry(getIndex());
            } else {
                next = new SearchHigh(this);
                next.setLow1(getPrevious());
            }
        }
        return next;
    }
}

package kds.chart;

public class HigherLow {

    private State state = new Start();

    public void process(int price) {
        state = state.process(price);
    }

    public int getEntry() {
        return state.getEntry();
    }

    public static void main(String[] args) {
        HigherLow hl = new HigherLow();
        for (String s : args) {
            int price = Integer.valueOf(s);
            hl.process(price);
        }
        System.out.println("Entry: " + hl.getEntry());
    }

}

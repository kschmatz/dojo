package kds.tdd;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class RationalNumberCalculatorTest {

	@Test
	public void testAddingTwoValues() {
		RationalNumber a = new RationalNumber(1, 3);
		RationalNumber b = new RationalNumber(3, 4);
		RationalNumber c = new RationalNumber(13, 12);
		assertEquals(c, RationalNumber.add(a, b));
	}

	@Test
	public void testAddingTwoValuesWithCommonDenominator() {
		RationalNumber a = new RationalNumber(1, 2);
		RationalNumber b = new RationalNumber(3, 4);
		RationalNumber c = new RationalNumber(5, 4);
		assertEquals(c, RationalNumber.add(a, b));
	}

	@Test
	public void testAddingTwoNegativeValues() {
		RationalNumber a = new RationalNumber(-1, 2);
		RationalNumber b = new RationalNumber(-3, 4);
		RationalNumber c = new RationalNumber(-5, 4);
		assertEquals(c, RationalNumber.add(a, b));
	}

	@Test(expected = ArithmeticException.class)
	public void testZeroDenominatorThrowsException() {
		new RationalNumber(1, 0);
	}
	
	@Test
	public void testMultiplyingTwoValues() {
		RationalNumber a = new RationalNumber(5, 6);
		RationalNumber b = new RationalNumber(4, 15);
		RationalNumber c = new RationalNumber(2, 9);
		assertEquals(c, RationalNumber.multiply(a, b));
	}

	@Test
	public void testTwoValuesAreEqual() {
		RationalNumber a = new RationalNumber(1, 2);
		RationalNumber b = new RationalNumber(4, 8);
		RationalNumber c = new RationalNumber(3, 6);
		assertTrue(a.equals(b));
		assertTrue(b.equals(c));
		assertTrue(a.equals(c));
	}

	@Test
	public void testTwoValuesAreNotEqual() {
		RationalNumber a = new RationalNumber(1, 2);
		RationalNumber b = new RationalNumber(2, 8);
		assertFalse(a.equals(b));
	}

	@Test
	public void testGreatestCommonDivisor() {
		assertEquals(1, RationalNumberHelper.greatestCommonDivisor(1, 1));
		assertEquals(1, RationalNumberHelper.greatestCommonDivisor(7, 13));
		assertEquals(1, RationalNumberHelper.greatestCommonDivisor(13, 9));
		assertEquals(2, RationalNumberHelper.greatestCommonDivisor(4, 6));
		assertEquals(2, RationalNumberHelper.greatestCommonDivisor(8, 6));
		assertEquals(10, RationalNumberHelper.greatestCommonDivisor(10, 20));
		assertEquals(10, RationalNumberHelper.greatestCommonDivisor(30, 10));
	}

	@Test
	public void testLeastCommonMultiple() {
		assertEquals(12, RationalNumberHelper.leastCommonMultiple(4, 6));
	}
}

package kds.tdd;

public class RationalNumber {

	private final int mNumerator;
	private final int mDenominator;

	public RationalNumber(int numerator, int denominator) {
		if (denominator == 0) {
			throw new ArithmeticException("denominator is zero");
		}

		final int gcd = RationalNumberHelper.greatestCommonDivisor(numerator,
				denominator);

		mNumerator = numerator / gcd;
		mDenominator = denominator / gcd;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof RationalNumber) {
			final RationalNumber o = (RationalNumber) other;
			return (mNumerator == o.mNumerator)
					&& (mDenominator == o.mDenominator);
		} else {
			return false;
		}

	}

	@Override
	public String toString() {
		return mNumerator + "/" + mDenominator;
	}

	public static RationalNumber add(RationalNumber a, RationalNumber b) {
		final int common = a.mDenominator * b.mDenominator;
		return new RationalNumber(a.mNumerator * common / a.mDenominator
				+ b.mNumerator * common / b.mDenominator, common);
	}

	public static RationalNumber multiply(RationalNumber a, RationalNumber b) {
		return new RationalNumber(a.mNumerator * b.mNumerator, a.mDenominator
				* b.mDenominator);
	}
}

package kds.puzzle;


public class Puzzle {
	static private int WIDTH = 4;

	private Tile[] mTiles = new Tile[WIDTH * WIDTH];
	
	private int empty;
	
	public Puzzle() {
		for (int i = 0; i < mTiles.length - 1; i++)
			mTiles[i] = Tile.create(i + 1);
		empty = mTiles.length - 1;
	}
	
	public Tile getTile(int i) {
		if (!isValidPosition(i))
			throw new IllegalArgumentException("position out of range: " + i);
		return mTiles[i];
	}

	public boolean isEmpty(int i) {
		return i == empty;
	}
	
	private static boolean isValidPosition(int i) {
		return i >= 0 && i < WIDTH * WIDTH;
	}
	
}

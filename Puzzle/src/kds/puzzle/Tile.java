package kds.puzzle;

public class Tile {

	private final int mId;

	public static Tile create(int id) {
		return new Tile(id);
	}

	private Tile(int id) {
		mId = id;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Tile))
			return false;
		Tile otherTile = (Tile) other;
		return mId == otherTile.mId;
	}

	@Override
	public int hashCode() {
		return mId;
	}

	public int id() {
		return mId;
	}
}

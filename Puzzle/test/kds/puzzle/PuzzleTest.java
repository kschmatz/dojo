package kds.puzzle;

import static org.junit.Assert.*;

import org.junit.Test;

public class PuzzleTest {

	@Test
	public void testPuzzleInitialized() {
		Puzzle p = new Puzzle();
		assertEquals(Tile.create(5), p.getTile(4));
		assertFalse(p.isEmpty(4));
		assertTrue(p.isEmpty(15));
	}

}

package kds.puzzle;

import static org.junit.Assert.*;

import org.junit.Test;

public class TileTest {

	@Test
	public void testTileRetainsId() {
		assertEquals(4, Tile.create(4).id());
	}
	
	@Test
	public void testEquals() {
		assertEquals(Tile.create(4), Tile.create(4));
	}
}
